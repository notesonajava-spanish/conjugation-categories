FROM adoptopenjdk/openjdk11:jre-11.0.2.9-alpine
ADD build/libs/conjugation-categories.jar app.jar

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Dspring.profiles.active=docker -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]
