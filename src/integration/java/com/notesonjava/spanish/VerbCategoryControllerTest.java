package com.notesonjava.spanish;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.rules.ExternalResource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoDatabase;
import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.CommandRunner;
import com.notesonjava.commands.compose.DockerComposeCommands;
import com.notesonjava.commands.compose.DockerComposeRule;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.filtering.model.VerbCategory;
import com.notesonjava.spanish.categories.controllers.VerbCategoryController;
import com.notesonjava.spanish.categories.repositories.VerbCategoryRepository;

import io.javalin.Javalin;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Ignore
public class VerbCategoryControllerTest {


	private static MongoClient client;
	private static ObjectMapper mapper;

	private static Javalin app;
	
	private static boolean useCompose;
	
	@ClassRule
	public static final ExternalResource resource = new DockerComposeRule("docker-compose-db.yml");	
	
	
	@BeforeClass
	public static void init() throws InterruptedException, IOException {
		
		PropertyMap props = PropertyLoader.loadProperties();
		
		useCompose = Boolean.parseBoolean(props.get("use.compose").orElse("true"));
		if(useCompose) {
			CommandResult restoreResult = CommandRunner.execute("mongorestore", "--db", "categories", "dump");
			restoreResult.getOutput().forEach(s -> log.info(s));
			Thread.sleep(1000);
			
		}
		
		
		CommandResult composeResult = DockerComposeCommands.up("docker-compose-db.yml");
		try {
			composeResult.getOutput().forEach(s -> log.info(s));
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (composeResult.isSuccess()) {
	
		}
		
		client = MongoClients.create();
		mapper = new ObjectMapper();
		
		app = Javalin.create();
		MongoDatabase database = client.getDatabase("categories");
		VerbCategoryRepository repo = new VerbCategoryRepository(database, mapper);
		VerbCategoryController controller = new VerbCategoryController(repo, mapper);
		controller.initMvc(app);
		app.start(4567);
		
	}
	
	@AfterClass
	public static void terminate() {
		client.close();
		app.stop();
	}

	@Test
	public void findAll() throws JsonParseException, JsonMappingException, IOException {
		
		HttpResponse response = HttpRequest.get("http://localhost:4567/categories/").send();
		List<VerbCategory> result = Arrays.asList(mapper.readValue(response.bodyText(), VerbCategory[].class));
		
		Assertions.assertThat(result).hasSize(170);
		
	}
	

	@Test
	public void findByTiempo() throws JsonParseException, JsonMappingException, IOException {
		
		int tiempoValue = Tiempo.IndicativoPresente.getValue();
		HttpResponse response = HttpRequest.get("http://localhost:4567/categories/tiempo/"+tiempoValue).send();
		List<VerbCategory> result = Arrays.asList(mapper.readValue(response.bodyText(), VerbCategory[].class));
		Assertions.assertThat(result).hasSize(40);
		result.forEach(vc -> Assertions.assertThat(vc.getTiempo()).isEqualTo(Tiempo.IndicativoPresente));
		
	}

	
}
