package com.notesonjava.spanish;

import java.io.IOException;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

import org.assertj.core.api.Assertions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.ExternalResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoDatabase;
import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.CommandRunner;
import com.notesonjava.commands.compose.DockerComposeRule;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.filtering.model.VerbCategory;
import com.notesonjava.spanish.categories.repositories.VerbCategoryRepository;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VerbCategoryRepositoryTest {

	private static VerbCategoryRepository repo;

	private static MongoClient client;
	
	private static boolean useCompose;
	
	@ClassRule
	public static final ExternalResource resource = new DockerComposeRule("docker-compose-db.yml");	
	

	@BeforeClass
	public static void init() throws InterruptedException, IOException {
		
		PropertyMap props = PropertyLoader.loadProperties();
		String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		
		
		useCompose = Boolean.parseBoolean(props.get("use.compose").orElse("true"));
		if(useCompose) {
			CommandResult restoreResult = CommandRunner.execute("mongorestore", "--host", dbHost, "--db", "categories", "dump");
			
			restoreResult.getOutput().forEach(s -> log.info(s));
			Thread.sleep(1000);
			
		}
		
		client = MongoClients.create("mongodb://"+dbHost+":"+dbPort);
		MongoDatabase database = client.getDatabase("categories");
		repo = new VerbCategoryRepository(database, new ObjectMapper());

	}

	@AfterClass
	public static void terminate() {
		client.close();
	}
	
	
	@Test
	public void findAll() {
		Flowable<VerbCategory> flow = repo.findAll();
		Set<VerbCategory> set = new HashSet<>();
		flow.blockingForEach(cat -> set.add(cat));

		long size = flow.count().blockingGet();
		Assertions.assertThat(size).isEqualTo(170);

	}

	@Test
	public void findByTiempo() {
		Flowable<VerbCategory> flow = repo.findByTiempo(Tiempo.Gerundio);
		Disposable dispose = flow.forEach(cat -> Assertions.assertThat(cat.getTiempo()).isEqualTo(Tiempo.Gerundio));
		Assertions.assertThat(flow.count().blockingGet()).isEqualTo(7l);
		Assertions.assertThat(dispose.isDisposed()).isTrue();
	}

	@Test
	public void findOne() {
		String id = "5b29461d68cde04c5f7e6af2";
		Maybe<VerbCategory> result = repo.findOne(id);
		result.doOnError(e -> Assertions.fail("Should find the id"))
				.doOnSuccess(vc -> Assertions.assertThat(vc.getId().getValue()).isEqualTo(id));
		VerbCategory category = result.blockingGet();
		Assertions.assertThat(category).isNotNull();
	}

	@Test
	public void findInvalidId() {

		repo.findOne("abcd").doOnError(e -> Assertions.assertThatExceptionOfType(Exception.class))
				.doOnSuccess(vc -> Assertions.fail("Should not succeed"));
	}

	@Test
	public void findMissing() {
		String id = "4b29461d68cde04c5f7e6af2";

		VerbCategory cat = repo.findOne(id).doOnSuccess(vc -> {
			System.out.println("Success");
			Assertions.fail("Should not exist");
		}).doOnError(e -> {
			System.out.println("Error");
			Assertions.assertThatExceptionOfType(NoSuchElementException.class);
		}).blockingGet();
		Assertions.assertThat(cat).isNull();
	}

	@Test
	public void findRegulars() {
		Flowable<VerbCategory> flow = repo.findRegularByTiempo(Tiempo.Gerundio);
		flow.forEach(cat -> {
			Assertions.assertThat(cat.getTiempo()).isEqualTo(Tiempo.Gerundio);
			Assertions.assertThat(cat.isRegular()).isTrue();
			Assertions.assertThat(cat.isComplex()).isFalse();

		});
		Assertions.assertThat(flow.count().blockingGet()).isEqualTo(2);
	}

	@Test
	public void findIrregulars() {
		Flowable<VerbCategory> flow = repo.findIrregularByTiempo(Tiempo.Participio);
		flow.forEach(cat -> {
			Assertions.assertThat(cat.getTiempo()).isEqualTo(Tiempo.Participio);
			Assertions.assertThat(cat.isRegular()).isFalse();

		});
		Assertions.assertThat(flow.count().blockingGet()).isEqualTo(11);


	}

	@Test
	public void findComplex() {
		Flowable<VerbCategory> flow = repo.findComplexByTiempo(Tiempo.IndicativoPresente);
		flow.forEach(cat -> {
			Assertions.assertThat(cat.getTiempo()).isEqualTo(Tiempo.IndicativoPresente);
			Assertions.assertThat(cat.isComplex()).isTrue();

		});
		Assertions.assertThat(flow.count().blockingGet()).isEqualTo(8);

	}

}
