package com.notesonjava.conjugations;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Filters.not;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.async.client.FindIterable;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;
import com.notesonjava.filtering.ConjugationMatcher;
import com.notesonjava.filtering.FilterResult;
import com.notesonjava.filtering.VerbStructure;
import com.notesonjava.filtering.VerbStructureFactory;
import com.notesonjava.filtering.model.ConjugationFilterChain;
import com.notesonjava.filtering.model.VerbCategory;
import com.notesonjava.mongo.reactive.AsyncAdapter;

import io.reactivex.Flowable;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Slf4j
public class ConjugationRepository {

	private MongoDatabase database;
	private DocumentAdapter adapter;
	
	public Flowable<VerbConjugation> findByCategory(VerbCategory category){
    	MongoCollection<Document> collection = database.getCollection("conjugations");
		Bson projection = Projections.fields(Projections.excludeId());
    	Bson filter = Filters.eq("tiempo", category.getTiempo().getValue());
    	
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
    	
    	
    	return AsyncAdapter.toFlowable(findIter)
				.map(doc -> adapter.convert(doc))
				.filter(vc -> { 
					VerbStructure structure = VerbStructureFactory.splitRoot(vc.getVerb());					
					Flowable<ConjugationFilterChain> filters = Flowable.fromIterable(category.getFilters());
					return  filters.any(filterChain -> applyFilterChain(filterChain, structure, vc)).blockingGet();
					
				})
				.doOnNext(vc -> log.info("Match : " + vc.toString() ));
		
			
	}
	
	public Flowable<VerbConjugation> findByFilterChain(ConjugationFilterChain filterChain, Tiempo tiempo){
    	MongoCollection<Document> collection = database.getCollection("conjugations");
		Bson projection = Projections.fields(Projections.excludeId());
    	Bson filter = Filters.eq("tiempo", tiempo.getValue());
    	
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
        	
    	return AsyncAdapter.toFlowable(findIter)
				.map(doc -> adapter.convert(doc))
				//.filter(vc -> vc.getVerb().equals("ir"))
				.filter(vc -> { 
					VerbStructure structure = VerbStructureFactory.splitRoot(vc.getVerb());		
					FilterResult result = filterChain.apply(structure);
					if(result.isMatch()) {
						return ConjugationMatcher.match(vc, result.getMapping(), structure);
					}
					return false;
					 
				});
		
			
	}

	
	
    public Flowable<VerbConjugation> findByTiempo(Tiempo tiempo){
    	MongoCollection<Document> collection = database.getCollection("conjugations");
		Bson projection = Projections.fields(Projections.excludeId());
    	Bson filter = Filters.eq("tiempo", tiempo.getValue());
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
        return AsyncAdapter.toFlowable(findIter)
				.map(doc -> adapter.convert(doc));
			
	}


    public Flowable<VerbConjugation> findByVerbAndTiempo(String verb, Tiempo tiempo) {
    
    	MongoCollection<Document> collection = database.getCollection("conjugations");
    	Bson projection = Projections.excludeId();
		Bson filter = Filters.and(Filters.eq("verb", verb), Filters.eq("tiempo", tiempo.getValue()));
		FindIterable<Document> findIter = collection.find(filter).projection(projection);
	    return AsyncAdapter.toFlowable(findIter).map(doc -> adapter.convert(doc));
    }
    
    public Flowable<VerbConjugation> findByTiempoAndVerbIn(Tiempo tiempo, Collection<String> verbs){

    	MongoCollection<Document> collection = database.getCollection("conjugations");
    	Bson projection = Projections.excludeId();
    	Bson filter = Filters.and(Filters.eq("tiempo", tiempo.getValue()), Filters.in("verb", verbs));
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
        return AsyncAdapter.toFlowable(findIter).map(doc -> adapter.convert(doc));
    }

    public Flowable<VerbConjugation> findByTiempoAndVerbNotIn(Tiempo tiempo, Collection<String> verbs){

    	MongoCollection<Document> collection = database.getCollection("conjugations");
    	
    	Bson projection = Projections.excludeId();
    	
    	Bson filter = and(not(in("verb", verbs)), eq("tiempo", tiempo.getValue()));
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
        return AsyncAdapter.toFlowable(findIter).map(doc -> adapter.convert(doc));

    }

    public Flowable<VerbConjugation> findByTiempoInAndVerbIn(List<Tiempo> tiempos, List<String> verbs){
    	
    	MongoCollection<Document> collection = database.getCollection("conjugations");
    	List<Integer> tiempoList = tiempos.stream().map(t -> t.getValue()).collect(Collectors.toList());
        
    	Bson projection = Projections.excludeId();
    	Bson filter = Filters.and(in("verb", verbs), in("tiempo",tiempoList));
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
        return AsyncAdapter.toFlowable(findIter).map(doc -> adapter.convert(doc));

    }


    public Flowable<VerbConjugation> findByTiempoInAndVerbEndsWith(List<Tiempo> tiempos, String verbSuffix){
    	
    	MongoCollection<Document> collection = database.getCollection("conjugations");
    	List<Integer> tiempoList = tiempos.stream().map(t -> t.getValue()).collect(Collectors.toList());
    	Bson projection = Projections.excludeId();
    	Bson filter = Filters.and(Filters.regex("verb", verbSuffix +"$"), Filters.in("tiempo", tiempoList));
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
        return AsyncAdapter.toFlowable(findIter).map(doc -> adapter.convert(doc));
    	
    }
    
    private boolean applyFilterChain(ConjugationFilterChain chain, VerbStructure structure, VerbConjugation vc) {
		log.debug("***********");
		
		
		log.debug("Changes " + chain.getChanges());
		chain.getSuffixMap().getSuffixes().forEach(suffix -> log.debug(suffix.toString()));
		
		log.debug("---------------");
		
		log.debug("Verb " + vc.getVerb());
		vc.getConjugations().forEach(c -> log.debug(c.toString()));
		
		FilterResult filterResult = chain.apply(structure);
		
		log.debug("Filter result :  " + filterResult);
		if(filterResult.isMatch()){
			return ConjugationMatcher.match(vc, filterResult.getMapping(), structure);
		}
		return false;
	}
	
}
