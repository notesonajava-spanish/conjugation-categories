package com.notesonjava.conjugations;

import java.util.ArrayList;

import org.bson.Document;

import com.notesonjava.conjugations.model.Conjugation;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.conjugations.model.VerbConjugation;


public class DocumentAdapter {

	@SuppressWarnings("unchecked")
	public VerbConjugation convert(Document document) {
		
		//log.info(document.toJson());
		
		VerbConjugation vc = new VerbConjugation();
		vc.setVerb(document.getString("verb"));
		vc.setTiempo(Tiempo.valueOf(document.getInteger("tiempo")));
		ArrayList conjugationList = document.get("conjugations", ArrayList.class);
		if(conjugationList != null ) {
			conjugationList.forEach(obj -> {
				Document conjugationDoc = (Document) obj;
				
				Conjugation c = convertConjugation(conjugationDoc);
				vc.addConjugation(c);
			});
		}
		
		return vc;
		
	}

	
	private Conjugation convertConjugation(Document conjugationDoc) {
		Conjugation c = new Conjugation();
		c.setWord(conjugationDoc.getString("word"));
		c.setPersona(Persona.valueOf(conjugationDoc.getInteger("persona")));
		return c;
	}
	
}
