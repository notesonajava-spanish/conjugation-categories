package com.notesonjava.conjugations.model;

import lombok.Getter;

public enum Persona {
	
	NONE(0),
	YO(1),
	TU(2),
	VOS(4),
	EL(5),
	ELLO(7),
	NOSOTROS(8),
	VOSOTROS(10),
	ELLOS(13);
	
	
	@Getter
	private int value;

    Persona(int value) {
        this.value = value;
    }
	
	public static Persona valueOf(int value){
		for(Persona p : Persona.values()){
			if(p.getValue() == value){
				return p;
			}
		}
		return Persona.NONE;
	}
}
