package com.notesonjava.conjugations.model;

import java.util.Arrays;
import java.util.List;

public enum Personas {

	ALL(Persona.YO,Persona.TU, Persona.EL, Persona.NOSOTROS, Persona.ELLOS),
	ALL_IMPERATIVO(Persona.TU, Persona.EL, Persona.NOSOTROS, Persona.ELLOS),
	YO(Persona.YO),
	TU(Persona.TU),
	EL(Persona.EL),
	NOS(Persona.NOSOTROS),
	EXCLUDE_TU_IMPERATIVO(Persona.EL, Persona.NOSOTROS, Persona.ELLOS),
	
	EXCLUDE_NOS_IMPERATIVO(Persona.TU, Persona.EL, Persona.ELLOS),
	EXCLUDE_NOS(Persona.YO, Persona.TU, Persona.EL, Persona.ELLOS),
	EXCLUDE_YO_AND_NOS( Persona.TU, Persona.EL, Persona.ELLOS),
	EL_ELLOS( Persona.EL, Persona.ELLOS),
	YO_EL( Persona.YO, Persona.EL),
	
	NEUTRAL(Persona.NONE);
	
	
	private List<Persona> personas;
	
	private Personas(Persona...personas){
		this.personas = Arrays.asList(personas);
	}
	
	public List<Persona> getPersonas(){
		return personas;
	}
	
	public boolean contains(Persona p){
		return personas.contains(p);
	}
}
