package com.notesonjava.conjugations.model;

import lombok.Getter;

public enum Tiempo {
	NONE(0),
	IndicativoPresente(1),
	SubjuntivoPresente(2),
	ImperativoPresente(3),
	IndicativoPreteritoimperfecto(4),
	SubjuntivoPreteritoImperfecti(5),
	IndicativoPreteritoPerfectoSimple(6),
	SubjuntivoPreteritoImperfecto(7),
	IndicativoPreteritoPerfectoCompuesto(8),
	SubjuntivoPreteritoPerfecto(9),
	IndicativoPreteritoPluscuamp(10),
	SubjuntivoPreteritoPluscuamperfectoB(11),
	SubjuntivoPreteritoPluscuamperferfctoB(12),
	IndicativoPreteritoAnterior(13),
	IndicativoFuturo(14),
	SubjuntivoFuturo(15),
	SubjuntivoFuturoPerfecto(16),
	IndicativoCondicional(17),
	Infinitivo(18),
	Participio(19),
	Gerundio(20),
	IndicativoFuturoPerfecto(21),
	
	IndicativoCondicionalPerfecto(22),
	InfinitivoCompuesto(60),
	GerundioCompuesto(61);
	
	@Getter
	private int value;

    Tiempo(int value) {
        this.value = value;
    }
	
	public static Tiempo valueOf(int value){
		for(Tiempo t : Tiempo.values()){
			if(t.getValue() == value){
				return t;
			}
		}
		return Tiempo.NONE;
	}
	
		
}


