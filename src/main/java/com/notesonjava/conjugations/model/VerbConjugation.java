package com.notesonjava.conjugations.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class VerbConjugation {

	private String verb;
	private Tiempo tiempo;
	private List<Conjugation> conjugations = new ArrayList<>();
	
	public void addConjugation(Conjugation c) {
		conjugations.add(c);
		
	}
}
