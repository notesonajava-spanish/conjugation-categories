package com.notesonjava.filtering;

import java.util.ArrayList;
import java.util.List;

import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.filtering.model.ConjugationFilterChain;
import com.notesonjava.filtering.model.SuffixMap;
import com.notesonjava.filtering.model.WordChange;

public class ConjugationFilterBuilder {
	private SuffixMap suffixMap;
	
	private List<WordChange> changes = new ArrayList<>();
	
	private String prefix = "";
	private String example = "";
	
	
	
	public static ConjugationFilterBuilder from(String... suffixes){
		ConjugationFilterBuilder builder = new ConjugationFilterBuilder();
		builder.suffixMap = new SuffixMap();		
		builder.suffixMap.put(Persona.TU, suffixes[0]);
		builder.suffixMap.put(Persona.EL, suffixes[1]);
		builder.suffixMap.put(Persona.NOSOTROS, suffixes[2]);
		builder.suffixMap.put(Persona.ELLOS, suffixes[3]);
		return builder;
	}
	
	public static ConjugationFilterBuilder from(SuffixMap map){
		ConjugationFilterBuilder builder = new ConjugationFilterBuilder();
		builder.suffixMap = map;
		return builder;
	}
	
	
	
	public ConjugationFilterBuilder change(WordChange change){
		this.changes.add(change);
		return this;
	}
	
	
	public ConjugationFilterBuilder prefix(String prefix){
		this.prefix = prefix;
		return this;
	}
	
	public ConjugationFilterBuilder example(String example){
		this.example = example;
		return this;
	}

	
	public ConjugationFilterChain build(){
		ConjugationFilterChain chain = new ConjugationFilterChain();
		chain.setSuffixMap(suffixMap);
		chain.setPrefix(prefix);
		chain.setChanges(changes);
		chain.setExample(example);
		return chain;
	}
}
