package com.notesonjava.filtering;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.notesonjava.conjugations.model.Conjugation;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.VerbConjugation;

import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class ConjugationMatcher {

	private static String watchedVerb ="hablar";
	public static boolean match(VerbConjugation vc, Map<Persona, String> mapping, VerbStructure structure){
		if(vc.getConjugations().size() < mapping.size())
			return false;
		
		long result = Flowable.fromIterable(vc.getConjugations())
				.filter(c -> compareSuffix(mapping, c, vc.getVerb()))
				.doOnNext(c -> log.debug("Match suffix :  " + c.getWord()))
				.filter(c ->  comparePrefix(mapping, c, structure, vc.getVerb()))
				.doOnNext(c -> log.debug("Match prefix :  " + c.getWord()))
				.count().blockingGet();
		
		if(structure.getVerb().equals(watchedVerb)){
			log.debug("Found matches : "+ result + "/"+vc.getConjugations().size());
		}
		
		return result == mapping.size();
	}

	public static boolean comparePrefix(Map<Persona, String> mapping, Conjugation c, VerbStructure structure, String verb) {
		String word = removeAccents(c.getWord());
		
		String comparable = removeAccents(structure.getShortPrefix()+mapping.get(c.getPersona()));
		if(verb.equals(watchedVerb)){
			log.debug("Do " + c.getWord() + " equals "+ word );
		}
		log.debug("Compare with " + comparable);
		boolean result = comparable.equals(word);
		log.debug("Prefix, Do " + c.getWord() + " equals "+ word );
		log.debug("Prefix, Do comparable " + comparable + " equals "+ word );
		return result;
	}
	
	public static boolean compareSuffix(Map<Persona, String> mapping, Conjugation c, String verb){
		if(!mapping.containsKey(c.getPersona())) {
			return false;
		}
		String suffix = mapping.get(c.getPersona());
		String suffixWithoutAccents = removeAccents(suffix);
		if(verb.equals(watchedVerb)){
			log.debug("Do " + c.getWord() + " ends with "+ suffixWithoutAccents );
		}
		boolean result =  removeAccents(c.getWord()).endsWith(suffixWithoutAccents);
		log.debug("Suffix, Do " + c.getWord() + " ends with "+ suffixWithoutAccents );
		return result;
	}
	
		
		
	private static String removeAccents(String word){
		return StringUtils.stripAccents(word).replaceAll("ü", "u");
	}
	
	/**
	 * Verify if the conjugation match the suffix map according to Future and Conditional pattern
	 * The pattern is the full verb with an extra suffix 
	 * ex : verb hablar in future is 'Yo hablare', hablar+'e' 
	 * 
	 * @param c
	 * @param suffixes
	 * @return
	 */
	public static boolean matchExtraSuffix(Conjugation c, Map<Persona, String> suffixes, String verb){
		String comparable = verb + suffixes.get(c.getPersona());
		return c.getWord().equals(comparable);
	}
}
