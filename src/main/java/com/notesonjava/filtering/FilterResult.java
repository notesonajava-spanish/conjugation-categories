package com.notesonjava.filtering;

import java.util.Map;

import com.notesonjava.conjugations.model.Persona;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FilterResult {

	private Map<Persona, String> mapping;
	private boolean match;
}
