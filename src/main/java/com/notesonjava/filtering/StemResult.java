package com.notesonjava.filtering;

import lombok.Data;

@Data
public class StemResult {
    private boolean match;
    private String stem;
}
