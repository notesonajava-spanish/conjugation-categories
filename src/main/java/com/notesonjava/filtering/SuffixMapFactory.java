package com.notesonjava.filtering;


import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.filtering.model.SuffixMap;

public class SuffixMapFactory {

	public static SuffixMap buildMap(String... endings) {
		SuffixMap map = new SuffixMap();
		map.put(Persona.YO, endings[0]);
		map.put(Persona.TU, endings[1]);
		map.put(Persona.EL, endings[2]);
		map.put(Persona.NOSOTROS, endings[3]);
		map.put(Persona.ELLOS, endings[4]);
		return map;
	}

	public static SuffixMap buildImperativo(String... endings) {
		SuffixMap map = new SuffixMap();
		
		map.put(Persona.TU, endings[0]);
		map.put(Persona.EL, endings[1]);
		map.put(Persona.NOSOTROS, endings[2]);
		map.put(Persona.ELLOS, endings[3]);
		return map;
	}
	
	public static SuffixMap buildParticipio(String ending) {
		
		SuffixMap map = new SuffixMap();
		map.put(Persona.NONE, ending);
		return map;
	}

}
