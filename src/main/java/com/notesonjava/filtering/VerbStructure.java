package com.notesonjava.filtering;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class VerbStructure {

	@NonNull
	private String verb;
	private String root;
	private String suffix;
	private String lastGroup;
	
	public boolean matchRoot(String pattern){
		return root.contains(pattern);
	}
	
	public String getLongSuffix(){
		return lastGroup+suffix;
	}
	
	public String getShortPrefix(){
		return root.substring(0, root.length() - lastGroup.length());
	}
	
}
