package com.notesonjava.filtering;

import java.util.Arrays;
import java.util.List;


public class VerbStructureFactory {

	
	private static final List<String> groupsNotVowel = Arrays.asList("gu");
	private static final List<String> vowels = Arrays.asList("e","o", "i", "u", "a"); 


	public static VerbStructure splitRoot(String verb) {
		return splitRoot(verb, 2);
	}
	
	public static VerbStructure splitRoot(String verb, String suffix) {
		return splitRoot(verb, suffix.length());
	}
	
	private static VerbStructure splitRoot(String verb, int suffixLength) {
		int separator = verb.length() - suffixLength;
		VerbStructure structure = new VerbStructure(verb);
		if(separator>=0){
			structure.setRoot(verb.substring(0, separator));
			structure.setSuffix(verb.substring(separator));
			
			int lastVowelIndex = getLastVowelIndex(structure.getRoot());
			if(lastVowelIndex > 0){
				structure.setLastGroup(structure.getRoot().substring(lastVowelIndex));
			}else{
				structure.setLastGroup(structure.getRoot());
			}
			
		}
		return structure;
	}
	
	private static int getLastVowelIndex(String word){
		int index = -1;
		for(String s: vowels){
			int current = word.lastIndexOf(s);
			int group = word.lastIndexOf(groupsNotVowel.get(0));
			if(current>0 && (current-group) == 1){
				current = word.substring(0, group).lastIndexOf(s);
			}
			if(current > index)
				index = current;
		}
		
		return index;
	}
	
}
