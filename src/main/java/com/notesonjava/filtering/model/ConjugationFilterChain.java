package com.notesonjava.filtering.model;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.filtering.FilterResult;
import com.notesonjava.filtering.StemResult;
import com.notesonjava.filtering.VerbStructure;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConjugationFilterChain {
	
	@JsonProperty("_id")
	private ObjectId id;
	
	private SuffixMap suffixMap;
	
	private List<WordChange> changes = new ArrayList<>();
	
	private String prefix = "";
	private String example;
	
	@JsonIgnore
	public FilterResult apply(VerbStructure structure ){
		
		String root = prefix + structure.getLastGroup();
		Map<Persona, String> mapping = new HashMap<>();
		
		if(root.length()>0){
			for(Suffix entry : this.suffixMap.getSuffixes()){
				if(changes.size()>0){
					StemResult result = calculateStemChange(root, entry.getPersona());
					if(result.isMatch()){
						String value = result.getStem() + entry.getSuffix();				
						mapping.put(entry.getPersona(), value);
					}else{
						return new FilterResult(mapping, false);
					}
				}else{
					String value = root + entry.getSuffix();
					mapping.put(entry.getPersona(), value);
				}
			}
			return new FilterResult(mapping, true);
		}
		return new FilterResult(mapping, false);
		
	}

	private StemResult calculateStemChange(String root, Persona p) {
		List<WordChange> filteredChange = changes.stream()
				.filter(change -> change.getPersonas().contains(p))
				.collect(toList());
		
		StemResult result = new StemResult();
		result.setStem(root);
		result.setMatch(true);
		for(WordChange change : filteredChange){	
			if(change.match(result.getStem())){
				result.setStem(change.apply(p, result.getStem()));	
			}else{
				result.setMatch(false);
				break;
			}					
		}
		return result;
	}
	
	
}

