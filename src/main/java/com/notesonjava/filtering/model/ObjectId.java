package com.notesonjava.filtering.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ObjectId {
	@JsonProperty("$oid")
	private String value;

}
