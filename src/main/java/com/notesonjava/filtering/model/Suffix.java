package com.notesonjava.filtering.model;

import com.notesonjava.conjugations.model.Persona;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Suffix {
	
	@NonNull
	private Persona persona;
	@NonNull
	private String suffix;
	
}
