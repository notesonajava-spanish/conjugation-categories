package com.notesonjava.filtering.model;

import java.util.ArrayList;
import java.util.List;

import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Tiempo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SuffixMap {

	
	
	private Tiempo tiempo;
	
	private boolean regular;
	
	private List<Suffix> suffixes = new ArrayList<>();
	
	public String get(Persona persona){
		return suffixes.stream().filter(suffix -> suffix.getPersona() == persona).findFirst().get().getSuffix();	
	}
	
	public void put(Persona persona, String suffix){
		suffixes.add(new Suffix(persona, suffix));
	}
}
