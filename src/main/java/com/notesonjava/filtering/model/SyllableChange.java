package com.notesonjava.filtering.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class SyllableChange {
	
	
	@NonNull
	private String from;
	
	@NonNull
	private String to;
	
	public String apply(String word){
		return word.replace(from, to);
	}

}
