package com.notesonjava.filtering.model;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.notesonjava.conjugations.model.Tiempo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class VerbCategory {
	
	@JsonProperty("_id")
	private ObjectId id;
	
	@NonNull
	private Tiempo tiempo;
	
	@NonNull
	private List<ConjugationFilterChain> filters;
	
	private boolean regular;
	
	
	public VerbCategory(Tiempo tiempo, ConjugationFilterChain filter, boolean regular){
		this.tiempo = tiempo;
		this.filters = Arrays.asList(filter);
		this.regular = regular;
	}
	
	@JsonIgnore
	public List<ConjugationFilterChain> getComplexFilters(){
		return filters.stream().filter(f -> f.getChanges().size() > 1).collect(toList());
	}
	
	@JsonIgnore
	public List<ConjugationFilterChain> getSimpleFilters(){
		return filters.stream().filter(f -> f.getChanges().size() == 1).collect(toList());
	}
	
	@JsonIgnore
	public boolean isComplex(){
		return getComplexFilters().size()>0;
	}
	
	
	
}
