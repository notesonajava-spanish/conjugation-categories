package com.notesonjava.filtering.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class VerbGrouping {
	
	@NonNull
	private VerbCategory category;
	
	private Set<String> verbs = new HashSet<>();


	@JsonIgnore
	public void add(String verb) {
		this.verbs.add(verb);
	}
	
	@JsonIgnore
	public void add(Collection<String> verbs) {
		this.verbs.addAll(verbs);
	}
}
