package com.notesonjava.filtering.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.notesonjava.conjugations.model.Persona;
import com.notesonjava.conjugations.model.Personas;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class WordChange {

	
	@NonNull
	private SyllableChange change;
	
	@NonNull
	private Personas personas;
		
	@NonNull
	private WordChangeType type;
	
	
	@JsonIgnore
	public String apply(Persona p, String word){
		if(personas.contains(p)){
			return change.apply(word);
		}
		return word;
	}
	
	@JsonIgnore
	public boolean match(String word){
		if(type == WordChangeType.Sound){
			return word.endsWith(change.getFrom());	
		}	
		return word.startsWith(change.getFrom());
	}
	
}
