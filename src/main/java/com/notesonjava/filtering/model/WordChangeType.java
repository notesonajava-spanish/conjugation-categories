package com.notesonjava.filtering.model;

public enum WordChangeType {

	Stem,
	Sound;
}
