package com.notesonjava.spanish.categories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoDatabase;
import com.notesonjava.spanish.categories.controllers.VerbCategoryController;
import com.notesonjava.spanish.categories.repositories.VerbCategoryRepository;

import io.javalin.Javalin;

public class CategoryApplication {


    public static void main(String[] args) {

    	MongoClient client = MongoClients.create();
    	MongoDatabase database = client.getDatabase("conjugations");
    	ObjectMapper mapper = new ObjectMapper();
    	
    	VerbCategoryRepository repo = new VerbCategoryRepository(database, mapper);
   
    	VerbCategoryController controller = new VerbCategoryController(repo, mapper);
    	Javalin app = Javalin.create();
    	controller.initMvc(app);
    	app.start(4567);
    	
    
    }
}
