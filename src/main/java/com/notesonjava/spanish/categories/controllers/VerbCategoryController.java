package com.notesonjava.spanish.categories.controllers;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.filtering.model.VerbCategory;
import com.notesonjava.spanish.categories.repositories.VerbCategoryRepository;

import io.javalin.Javalin;
import io.reactivex.Maybe;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@AllArgsConstructor
public class VerbCategoryController {
	   private static final String APPLICATION_JSON_UTF8 = "application/json;charset=UTF-8";

	private VerbCategoryRepository repo;
	
	private ObjectMapper mapper;
	public void initMvc(Javalin app) {
	
		app.get("/categories",  ctx -> {
			ctx.status(200);
			ctx.header("Content-Type", APPLICATION_JSON_UTF8);
			List<VerbCategory> list = repo.findAll().toList().blockingGet();
			list.forEach(System.out::println);
			String result = mapper.writeValueAsString(list);
			ctx.result(result);
			
		});
		app.get("/categories/:id",  ctx -> {
			ctx.header("Content-Type", APPLICATION_JSON_UTF8);
			String id = ctx.pathParam(":id");
			Maybe<VerbCategory> option = repo.findOne(id);
			Maybe<VerbCategory> maybe = option.doOnSuccess(vc -> {
				ctx.status(200);
				ctx.result(mapper.writeValueAsString(vc));
			}).doOnError(err -> {
				ctx.status(204);
			});
			
		});
		app.get("/categories/tiempo/:tiempo", ctx -> {
			ctx.status(200);
			ctx.header("Content-Type", APPLICATION_JSON_UTF8);
			Tiempo tiempo = convertTiempo(ctx.pathParam(":tiempo"));
			String result = mapper.writeValueAsString(repo.findByTiempo(tiempo).toList().blockingGet());
			ctx.result(result);
		});
		app.get("/categories/regular/:tiempo", ctx -> {
			ctx.header("Content-Type", APPLICATION_JSON_UTF8);
			
			Tiempo tiempo = convertTiempo(ctx.pathParam(":tiempo"));
			String result = mapper.writeValueAsString(repo.findRegularByTiempo(tiempo).toList().blockingGet());
			ctx.result(result);
		});
		
		app.get("/categories/irregulars/:tiempo", ctx -> {
			ctx.header("Content-Type", APPLICATION_JSON_UTF8);
			
			Tiempo tiempo = convertTiempo(ctx.pathParam(":tiempo"));
			String result = mapper.writeValueAsString(repo.findRegularByTiempo(tiempo).toList().blockingGet());
			ctx.result(result);
		});
			
		app.get("/categories/complex/:tiempo", ctx -> {
			ctx.header("Content-Type", APPLICATION_JSON_UTF8);
			
			Tiempo tiempo = convertTiempo(ctx.pathParam(":tiempo"));
			String result = mapper.writeValueAsString(repo.findRegularByTiempo(tiempo).toList().blockingGet());
			ctx.result(result);
		});
	
	}
	
	private Tiempo convertTiempo(String tiempoValue) {
		Integer tiempoId = Integer.parseInt(tiempoValue);
		return Tiempo.valueOf(tiempoId);
	}
    
	
}
