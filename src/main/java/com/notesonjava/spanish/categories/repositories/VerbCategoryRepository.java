package com.notesonjava.spanish.categories.repositories;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.FindIterable;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.notesonjava.conjugations.model.Tiempo;
import com.notesonjava.filtering.model.VerbCategory;
import com.notesonjava.mongo.reactive.AsyncAdapter;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbCategoryRepository {
	
	private static final String COLLECTION = "categories";
	
	private MongoDatabase database;
	private ObjectMapper mapper;

	public Flowable<VerbCategory> findAll(){
		MongoCollection<Document> collection = database.getCollection(COLLECTION);
		Bson projection = Projections.fields();
    	
    	FindIterable<Document> findIter = collection.find().projection(projection);
    	
    	return AsyncAdapter.toFlowable(findIter)
    			.map(doc -> mapper.readValue(doc.toJson(), VerbCategory.class));
		
	}
	
	
	public Maybe<VerbCategory> findOne(String hexid){
		MongoCollection<Document> collection = database.getCollection(COLLECTION);
		try {
			ObjectId id = new ObjectId(hexid);
		    
			
			Bson projection = Projections.fields();
			Bson filter = Filters.eq("_id", id);
	    	
	    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
	    	
	    	Flowable<VerbCategory> flow = AsyncAdapter.toFlowable(findIter).map(doc -> mapper.readValue(doc.toJson(), VerbCategory.class));
	    	Maybe<VerbCategory> maybe = flow.firstElement();
	    	return maybe;
	    	
			
		} catch (Exception e ) {
			return Maybe.error(e);
		}
	}
	
	
	
	public Flowable<VerbCategory> findByTiempo(Tiempo tiempo){
		MongoCollection<Document> collection = database.getCollection(COLLECTION);
		
		Bson projection = Projections.fields();
    	Bson filter = Filters.eq("tiempo", tiempo.toString());
    	
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
    	
    	return AsyncAdapter.toFlowable(findIter)
    			.map(doc -> mapper.readValue(doc.toJson(), VerbCategory.class));
		
	}
	
	public Flowable<VerbCategory> findRegularByTiempo(Tiempo tiempo){
		MongoCollection<Document> collection = database.getCollection(COLLECTION);
		
		Bson projection = Projections.fields();
    	Bson filter = Filters.and(Filters.eq("regular", true) ,Filters.eq("tiempo", tiempo.toString()));
    	
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
    	
    	return AsyncAdapter.toFlowable(findIter)
    			.map(doc -> mapper.readValue(doc.toJson(), VerbCategory.class));	
	}
	
	public Flowable<VerbCategory> findIrregularByTiempo(Tiempo tiempo){
		MongoCollection<Document> collection = database.getCollection(COLLECTION);
		
		Bson projection = Projections.fields();
    	Bson filter = Filters.and(Filters.eq("regular", false) ,Filters.eq("tiempo", tiempo.toString()));
    	
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
    	
    	return AsyncAdapter.toFlowable(findIter)
    			.map(doc -> mapper.readValue(doc.toJson(), VerbCategory.class));
		
	}
	
	public Flowable<VerbCategory> findComplexByTiempo(Tiempo tiempo){
		MongoCollection<Document> collection = database.getCollection(COLLECTION);
		
		Bson projection = Projections.fields();
    	Bson filter = Filters.and(Filters.eq("regular", false) ,Filters.eq("tiempo", tiempo.toString()));
    	
    	FindIterable<Document> findIter = collection.find(filter).projection(projection);
    	
    	return AsyncAdapter.toFlowable(findIter)
    			.map(doc -> mapper.readValue(doc.toJson(), VerbCategory.class))
    			.filter(vc -> !vc.getComplexFilters().isEmpty());
		
	}
	
	
	public Completable save(VerbCategory category) {
		MongoCollection<Document> collection = database.getCollection(COLLECTION);
		try {
			String groupJson = mapper.writeValueAsString(category);
			Document doc = Document.parse(groupJson);
			
			SingleResultCallback<Void> callback = new SingleResultCallback<Void>() {

				@Override
				public void onResult(Void result, Throwable t) {
					log.info("Saved");
				}
			};
			collection.insertOne(doc, callback );
			return Completable.complete();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return Completable.error(e);
		}	  	
	}

}
